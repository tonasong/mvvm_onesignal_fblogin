package com.example.fbloginmvvm.helper;

import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONObject;

public class OneSignalHelper {

    public static void pushNotificationByPlayerID(String[] playerId){

        //create jsonObject
        JSONObject messageObj = new JSONObject();
        JSONArray playerIdArray = new JSONArray();
        JSONObject contentObj = new JSONObject();
        JSONObject headingObj = new JSONObject();

        for(int i=0;i<playerId.length;i++)
            playerIdArray.put(playerId[i]);
        try{

            contentObj.put("en","You got 100$");
            headingObj.put("en","Congratulation");

            messageObj.put("app_id","25654653-adef-40a6-947c-fb251c9f1225");
            messageObj.put("include_player_ids",playerIdArray);
            messageObj.put("headings",headingObj);
            messageObj.put("contents",contentObj);

        }catch (Exception ex){ex.printStackTrace();}

        OneSignal.postNotification(messageObj, new OneSignal.PostNotificationResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                System.out.println("sucess........"+response.toString());
            }

            @Override
            public void onFailure(JSONObject response) {
                System.out.println("OnError"+response.toString());
            }
        });

    }
}
